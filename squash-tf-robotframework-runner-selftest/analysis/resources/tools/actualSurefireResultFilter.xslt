<?xml version="1.0" encoding="utf-8"?>
<!--

        This file is part of the Squashtest platform.
        Copyright (C) 2019 - 2020 Henix

        See the NOTICE file distributed with this work for additional
        information regarding copyright ownership.

        This is free software: you can redistribute it and/or modify
        it under the terms of the GNU Lesser General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        this software is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Lesser General Public License for more details.

        You should have received a copy of the GNU Lesser General Public License
        along with this software.  If not, see <http://www.gnu.org/licenses />.

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" encoding="utf-8"/>
    <xsl:template match="/">
            <xsl:apply-templates select="node()"/>
    </xsl:template>
    <xsl:template match="*|@*">
        <xsl:message>Matching : <xsl:value-of select="name()"/></xsl:message>
        <xsl:copy>
            <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
    </xsl:template>
    <xsl:template match="testsuite">
        <xsl:copy>
            <xsl:for-each select="testcase">
                <xsl:sort order="ascending" select="./@name"/>
                <xsl:apply-templates select="." />
            </xsl:for-each>
        </xsl:copy>
    </xsl:template>
    <xsl:template match="testcase">
        <xsl:element name="{translate(@name,' ','_')}">
              <xsl:apply-templates select="@*|failure"/>
              <xsl:if test="count(./failure)=0">
                  <xsl:attribute name="status">SUCCESS</xsl:attribute>
              </xsl:if>
        </xsl:element>
    </xsl:template>
    <xsl:template match="failure">
        <xsl:attribute name="status">FAILED</xsl:attribute>
        <xsl:copy>
            <xsl:copy-of select="./@*" />
        </xsl:copy>    
    </xsl:template>
    <xsl:template match="@time">
        <!-- Screw time, we can't control it! -->
    </xsl:template>
</xsl:stylesheet>