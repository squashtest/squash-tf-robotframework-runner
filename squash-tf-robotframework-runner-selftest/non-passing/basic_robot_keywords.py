#
#     This file is part of the Squashtest platform.
#     Copyright (C) 2019 - 2020 Henix
#
#     See the NOTICE file distributed with this work for additional
#     information regarding copyright ownership.
#
#     This is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Lesser General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     this software is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Lesser General Public License for more details.
#
#     You should have received a copy of the GNU Lesser General Public License
#     along with this software.  If not, see <http://www.gnu.org/licenses />.
#

class basic_robot_keywords(object):
    """Keyword library to test the RobotFramwork integration components in SKF

    Just a few hardcoded keywords to test our integration layer
    """

    def __init__(self):
        """Just an object init for the fun of it"""

    def happy_go_lucky_keyword(self):
        """Always succeeds
        """

    def camille_keyword(self, argument):
        """That one always says no, except if Milk is the matter. ;)
        """
        if argument == "Milk":
            return
        else:
            raise AssertionError('No ! Dont wanna %s' % (argument))