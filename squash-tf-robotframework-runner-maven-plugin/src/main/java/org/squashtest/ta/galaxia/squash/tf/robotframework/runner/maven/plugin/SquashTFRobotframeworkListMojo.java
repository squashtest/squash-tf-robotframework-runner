/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2019 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.squash.tf.robotframework.runner.maven.plugin;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.squashtest.ta.galaxia.robotframework.runner.exploitation.explorer.RobotFrameworkTestExplorer;
import org.squashtest.ta.galaxia.squash.tf.robotframework.runner.maven.json.listing.RobotFrameworkRunnerJsonTree;
import org.squashtest.ta.galaxia.robotframework.runner.exploitation.link.RobotFrameworkTestEcosystemModel;
import org.squashtest.ta.galaxia.robotframework.runner.exploitation.link.RobotFrameworkTestSpecParser;
import org.squashtest.ta.galaxia.robotframework.runner.exploitation.link.TestPointer;
import org.squashtest.ta.galaxia.squash.tf.robotframework.runner.maven.metadata.RobotFWRunnerMetadataSyntaxChecker;
import org.squashtest.ta.maven.AbstractBaseSquashTaMojo;

/**
 * Mojo to list Robotframework tests in a Robotframework project.
 * @author edegenetais
 */
@Mojo(
        name = "list",
        defaultPhase = LifecyclePhase.INTEGRATION_TEST,
        requiresProject = false
)
public class SquashTFRobotframeworkListMojo extends AbstractBaseSquashTaMojo{
      
    @Parameter (property="tf.disableMetadata")
    private boolean disableMetadata;

    @Override
    protected void execution() throws MojoExecutionException, MojoFailureException {
        //initialization sequence.
        getLogger().info("Initialization");
        initLogging();
        init();
        resetOutputDirectory();
        
        getLogger().info("Listing tests...");
        
        File projectBase=getBaseDir();
        
        RobotFrameworkTestSpecParser testSpecParser=new RobotFrameworkTestSpecParser(projectBase, "**/*");
        List<TestPointer> testPointers=testSpecParser.parseTestList();
        getLogger().debug(testPointers.size()+" tests found.");
        
        RobotFrameworkTestExplorer explorer = new RobotFrameworkTestExplorer();
        
        Map<String, List<TestPointer>> ecosystemGroups = new RobotFrameworkTestEcosystemModel(testPointers).groupPointersByEcosystemDefinition();       
        RobotFrameworkRunnerJsonTree robotFWRunnerJsonTree = new RobotFrameworkRunnerJsonTree(getOutputDirectory(), explorer, projectBase);
        
        if (disableMetadata){     
            robotFWRunnerJsonTree.write(ecosystemGroups, false);
        }else{

            getLogger().info("Squash TF: Metadata checking...");
            new RobotFWRunnerMetadataSyntaxChecker(robotFWRunnerJsonTree, ecosystemGroups, true, true, false).checkMetadataSyntaxByDefault();
            getLogger().info("Squash TF: Metadata checking successfully completed");
            robotFWRunnerJsonTree.write(ecosystemGroups, true);
        }
        
        getLogger().info("Done");
    }

    private void init() throws MojoExecutionException {
        try {
            new NoPomPluginInitializer(this).apply(getOutputDirectory(), getBaseDir());
        } catch (IOException ex) {
            throw new MojoExecutionException("Failure during mojo initialization phase. Configuration is probably wrong.",ex);
        }
    }

}
