/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2019 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.squash.tf.robotframework.runner.maven.plugin;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.galaxia.robotframework.runner.exploitation.explorer.RobotFrameworkTestExplorer;
import org.squashtest.ta.galaxia.robotframework.runner.exploitation.link.RobotFrameworkTestEcosystemModel;
import org.squashtest.ta.galaxia.robotframework.runner.exploitation.link.RobotFrameworkTestSpecParser;
import org.squashtest.ta.galaxia.robotframework.runner.exploitation.link.TestPointer;
import org.squashtest.ta.galaxia.squash.tf.robotframework.runner.maven.json.listing.RobotFrameworkRunnerJsonTree;
import org.squashtest.ta.galaxia.squash.tf.robotframework.runner.maven.metadata.RobotFWRunnerMetadataSyntaxChecker;
import org.squashtest.ta.maven.AbstractBaseSquashTaMojo;
import org.squashtest.ta.squash.ta.addon.logic.kit.MetadataSyntaxChecker;


/**
 * Mojo to check metadata in a RobotFramework test.
 * @author dclaerhout
 */
@Mojo(
        name = "check-metadata",
        defaultPhase = LifecyclePhase.INTEGRATION_TEST,
        requiresProject = false
)
public class SquashTFRobotFrameworkCheckMetadataMojo extends AbstractBaseSquashTaMojo{
    
    private static final Logger LOGGER = LoggerFactory.getLogger(SquashTFRobotFrameworkCheckMetadataMojo.class);
    
    @Parameter (property="tf.metadata.check")
    private String metadataCheckParam;
    
    @Parameter (property="tf.metadata.check.keys")
    private String metadataCheckKeyListParam;
    
    @Override
    protected void execution() throws MojoExecutionException, MojoFailureException {
        //initialization sequence.
        getLogger().info("Initialization");
        initLogging();
        init();
        resetOutputDirectory();
        
        getLogger().info("Listing tests...");
        
        File projectBase=getBaseDir();
        
        RobotFrameworkTestSpecParser testSpecParser=new RobotFrameworkTestSpecParser(projectBase, "**/*");
        List<TestPointer> testPointers=testSpecParser.parseTestList();
        getLogger().debug(testPointers.size()+" tests found.");
        RobotFrameworkTestExplorer explorer = new RobotFrameworkTestExplorer();
              
        Map<String, List<TestPointer>> ecosystemGroups = new RobotFrameworkTestEcosystemModel(testPointers).groupPointersByEcosystemDefinition();       
        RobotFrameworkRunnerJsonTree robotFWRunnerJsonTree = new RobotFrameworkRunnerJsonTree(getOutputDirectory(), explorer, projectBase);
        


        //********* execution ********
        
        if (null == metadataCheckParam){
  
            MetadataSyntaxChecker checker = new RobotFWRunnerMetadataSyntaxChecker(robotFWRunnerJsonTree,ecosystemGroups, true, true, false);
            getLogger().info("Squash TF: checking metadata...");            
            
            //default metadata syntax validation
            checker.checkMetadataSyntaxByDefault();
        } else {
            
            MetadataSyntaxChecker checker = new RobotFWRunnerMetadataSyntaxChecker(robotFWRunnerJsonTree,ecosystemGroups, true, true, true);
            getLogger().info("Squash TF: checking metadata...");   
  
            LOGGER.debug("User input for tf.metadata.check: \'"+metadataCheckParam+"\'.");
            String userInputForMetadataCheckParam = metadataCheckParam.trim();            
            
            if (isWrappedByBrackets(userInputForMetadataCheckParam)){
                treatUserInputListForMetadataCheckParam(userInputForMetadataCheckParam, checker);
            } else {
                raiseNonCoveredByBracketsArgExcptForMetadataCheckParam();
            }
        }
        getLogger().info("Squash TF: Metadata checking successfully completed");
    }
    
    private boolean isWrappedByBrackets(String userInputForMetadataCheckParam) {
        String regex = "\\[.*\\]";
        Pattern pat = Pattern.compile(regex);
        return pat.matcher(userInputForMetadataCheckParam).matches();
    }
    
    private void treatUserInputListForMetadataCheckParam(String userInputForMetadataCheckParam, MetadataSyntaxChecker checker) {
        String removedBracketsInput = removeBracketsOfInputString(userInputForMetadataCheckParam);
        String trimmedRemovedBracketsInput = removedBracketsInput.trim();
        if ("".equals(trimmedRemovedBracketsInput)){
            invokeEmptyUserInputListForMetadataCheckParamException();
        } else {
            treatNotEmptyUserInputListForMetadataCheckParam(removedBracketsInput, checker);
        }
    }
    
    private String removeBracketsOfInputString(String userInputForMetadataCheckParam) {
        return userInputForMetadataCheckParam.substring(1, userInputForMetadataCheckParam.length()-1);
    }
    
    private void invokeEmptyUserInputListForMetadataCheckParamException() {
        LOGGER.error("\'"+metadataCheckParam+"\': "+INVALID_METADATA_CHECK_PARAM
                +". Parameter input list MUST NOT be empty.");
        throw new IllegalArgumentException(INVALID_METADATA_CHECK_PARAM);
    }
    
    private void treatNotEmptyUserInputListForMetadataCheckParam(String removedBracketsInput, MetadataSyntaxChecker checker) {
        String[] inputList = removedBracketsInput.split(",");
        for (String input :  inputList){
            String trimmedInput =  input.trim();
            if ("".equals(trimmedInput)){
                invokeEmptyUserInputForMetadataCheckParamException(trimmedInput);
            } else {
                treatUserInputForMetadataCheckParam(trimmedInput, checker);
            }
        }        
    }
    
    private void invokeEmptyUserInputForMetadataCheckParamException(String trimmedInput) {
        LOGGER.error("\'"+trimmedInput+"\': "+INVALID_METADATA_CHECK_PARAM
                +". Parameter input MUST NOT be empty.");
        throw new IllegalArgumentException(INVALID_METADATA_CHECK_PARAM);
    }
    
    private void treatUserInputForMetadataCheckParam(String trimmedInput, MetadataSyntaxChecker checker) {
        switch (trimmedInput) {
            case "valueUnicity":
                getLogger().info("Squash TF: checking metadata with value unicity...");
                checker.checkMetadataSyntaxWithUnicityChecking(metadataCheckKeyListParam);
                break;
            case "keyUnicity":
                //TODO: Not implemented yet
                getLogger().info("Squash TF: checking metadata with key unicity...");
                break;
            default:
                raiseIllegalArgExcptForMetadataCheckParam(trimmedInput);
        }
    }
    
    private void raiseIllegalArgExcptForMetadataCheckParam(String trimmedInput) {
        LOGGER.error("\'"+trimmedInput+"\': "+INVALID_METADATA_CHECK_PARAM
                +". Parameter input is NOT defined.");
        throw new IllegalArgumentException(INVALID_METADATA_CHECK_PARAM);
    }
    
    private void raiseNonCoveredByBracketsArgExcptForMetadataCheckParam() {
        LOGGER.error("\'"+metadataCheckParam+"\': "+INVALID_METADATA_CHECK_PARAM
                +". Parameter input(s) must be wrapped in brackets, ex: \"[valueUnicity]\" or \"[valueUnicity, keyUnicity]\".");
        throw new IllegalArgumentException(INVALID_METADATA_CHECK_PARAM);
    }
    
    private void init() throws MojoExecutionException {
        try {
            new NoPomPluginInitializer(this).apply(getOutputDirectory(), getBaseDir());
        } catch (IOException ex) {
            throw new MojoExecutionException("Failure during mojo initialization phase. Configuration is probably wrong.",ex);
        }
    }
    
}
