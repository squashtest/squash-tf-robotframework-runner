/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2019 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.squash.tf.robotframework.runner.maven.metadata;


import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.metadata.MetadataCheckingResult;
import org.squashtest.ta.galaxia.robotframework.runner.exploitation.link.TestPointer;
import org.squashtest.ta.galaxia.squash.tf.robotframework.runner.maven.json.listing.RobotFrameworkRunnerJsonTree;
import org.squashtest.ta.plugin.robot.fw.library.RobotFWTestAnomaly;
import org.squashtest.ta.squash.ta.addon.logic.kit.MetadataSyntaxChecker;


/**
 *
 * @author dclaerhout
 */
public class RobotFWRunnerMetadataSyntaxChecker extends MetadataSyntaxChecker<TestPointer> {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(RobotFWRunnerMetadataSyntaxChecker.class);    
         
    private RobotFrameworkRunnerJsonTree robotFWRunnerJsonTree;
    
  
    public RobotFWRunnerMetadataSyntaxChecker(RobotFrameworkRunnerJsonTree robotFWRunnerJsonTree,
                                              Map<String, List<TestPointer>> ecosystemGroups,
                                              boolean invokeError,
                                              boolean checkValueUnicityInMultiValueMetadata,
                                              boolean withProjectValueUnicityChecking) {
        
        super(ecosystemGroups, invokeError, checkValueUnicityInMultiValueMetadata, withProjectValueUnicityChecking);
        this.robotFWRunnerJsonTree = robotFWRunnerJsonTree;
    }

    @Override
    protected MetadataCheckingResult hasMetadataErrorInTestPointer(String groupName, TestPointer testPointer){
        boolean hasSyntaxError = false;
        boolean hasUnicityError = false;
        boolean hasMetadataInEcoFileError = false;
        
        
        String testCaseName = testPointer.getTestName();
        Map<String, List<String>> metadataList = robotFWRunnerJsonTree.getMetadataForTestPointer(testPointer);
        List<RobotFWTestAnomaly> testAnomalies = robotFWRunnerJsonTree.getRobotFWTestAnomalies(testPointer);
        
        if (!testAnomalies.isEmpty()) {

            for (RobotFWTestAnomaly anomaly : testAnomalies) {
                if ("Duplicate_key".equals(anomaly.getType())) {
                    invokerMetadataError(groupName, testCaseName, anomaly.getMessage(), "] The current test method contains duplicates of Metadata KEY: \'");
                    hasSyntaxError = true;
                }
            }
        }
        
        for (Map.Entry<String, List<String>> metadata : metadataList.entrySet()){
   
            MetadataCheckingResult metadataResult = hasMetadataErrorInEachMetadata(groupName, testCaseName, metadata);
            hasSyntaxError = metadataResult.hasSyntaxError() || hasSyntaxError;
            hasUnicityError = metadataResult.hasUnicityError() || hasUnicityError;
            hasMetadataInEcoFileError = metadataResult.hasMetadataInEcoFileError() || hasMetadataInEcoFileError;           
        }
        return new MetadataCheckingResult(hasSyntaxError, hasUnicityError, hasMetadataInEcoFileError);
    }

    @Override
    protected void invokerMetadataError(String groupName, String testCaseName, String currentKey, String errorMsg)  {
        if (isInvokeError()) {
            LOGGER.error("[Test suite: {} - Test case: {} {} {} \'.",groupName,testCaseName,errorMsg,currentKey);
        } else {
            LOGGER.warn("[Test suite: {} - Test case: {} {} {} \'.",groupName,testCaseName,errorMsg,currentKey);
        }
    }

    @Override
    protected void invokeMultiValueDuplicate(String key, String currentValue, String groupName, String testCaseName)  {
        if (isCheckValueUnicityInMultiValueMetadata()){
             LOGGER.warn("[Test suite: {} - Test case: {}] Warning while parsing Metadata VALUE: \'{}\' - a same VALUE is already assigned to the current Metadata KEY: \'{}\'.",groupName, testCaseName, currentValue, key);      
        }    
    }
  
}
