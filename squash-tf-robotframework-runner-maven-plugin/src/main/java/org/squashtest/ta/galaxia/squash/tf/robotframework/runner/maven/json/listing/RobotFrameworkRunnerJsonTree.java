/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2019 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.squash.tf.robotframework.runner.maven.json.listing;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.squashtest.ta.galaxia.robotframework.runner.exploitation.explorer.RobotFrameworkTestExplorer;
import org.squashtest.ta.galaxia.robotframework.runner.exploitation.link.TestPointer;
import org.squashtest.ta.plugin.robot.fw.library.RobotFWTest;
import org.squashtest.ta.plugin.robot.fw.library.RobotFWTestAnomaly;
import org.squashtest.ta.plugin.robot.fw.library.RobotFWTestSuite;
import org.squashtest.ta.squash.ta.addon.logic.kit.AbstractRunnerJsonTree;

/**
 * This class implements the RobotFramework Runner version of the project object model
 * to Squash Json listing codec.
 * 
 * @author edegenetais
 */
public class RobotFrameworkRunnerJsonTree extends AbstractRunnerJsonTree<TestPointer>{
    
    private RobotFrameworkTestExplorer explorer;
    private File projectBase;
    
    private List<RobotFWTestSuite> projectModel;

    public RobotFrameworkRunnerJsonTree(File destinationDirectory, RobotFrameworkTestExplorer explorer, File projectBase) {
        super(destinationDirectory);
        this.explorer=explorer;
        this.projectBase=projectBase;
    }

    @Override
    protected String getTestPointerTestName(TestPointer testPointer) {
        return testPointer.getTestName();
    }
    
    @Override
    public Map<String, List<String>> getMetadataForTestPointer(TestPointer testPointer) {
        List<RobotFWTestSuite> allSuites = modelizeProject();
        Map<String, List<String>> metadataList = new HashMap<>();

        for (RobotFWTestSuite suite : allSuites) {
            if (testPointer.getSuiteName().equals(suite.getName())) {
                metadataList = checkAndLoadMetadatalistFromTestnameProvidedByTestpointer(suite, testPointer, metadataList);
            }
        }
        return metadataList;
    }

    private Map<String, List<String>> checkAndLoadMetadatalistFromTestnameProvidedByTestpointer(RobotFWTestSuite suite, TestPointer testPointer, Map<String, List<String>> metadataList) {
        for (RobotFWTest test : suite.getTests()) {
            if (testPointer.getTestName().equals(test.getName())) {
                metadataList = test.getMetadataList();
            }
        }
        return metadataList;
    }

    public List<RobotFWTestAnomaly> getRobotFWTestAnomalies(TestPointer testPointer) {
        List<RobotFWTestSuite> allSuites = modelizeProject();
        List<RobotFWTestAnomaly> testAnomalies = new ArrayList<>();

        for (RobotFWTestSuite suite : allSuites) {
            if (testPointer.getSuiteName().equals(suite.getName())) {
                testAnomalies = checkAndLoadTestAnomaliesFromTestnameProvidedByTestpointer(suite, testPointer, testAnomalies);
            }
        }
        return testAnomalies;
    }

    private List<RobotFWTestAnomaly> checkAndLoadTestAnomaliesFromTestnameProvidedByTestpointer(RobotFWTestSuite suite, TestPointer testPointer, List<RobotFWTestAnomaly> testAnomalies) {
        for (RobotFWTest test : suite.getTests()) {
            if (testPointer.getTestName().equals(test.getName())) {
                testAnomalies = test.getTestAnomalies();
            }
        }
        return testAnomalies;
    }

    protected List<RobotFWTestSuite> modelizeProject() {
        synchronized (this) {
            if (projectModel == null) {
                projectModel = explorer.getBaseTestSuite(projectBase);
            }
            return projectModel;
        }
    }

    /* testability Date management mechanisme. */
    static interface DateFactory extends AbstractRunnerJsonTree.DateFactory{};
    void setDateFactory(DateFactory f){super.dateFactory=f;};
    /* end of testability Date management mechanisme */
}
