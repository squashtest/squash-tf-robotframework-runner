/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2019 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.squash.tf.robotframework.runner.maven.plugin;

import java.io.File;
import java.io.IOException;
import org.apache.maven.plugin.logging.Log;
import org.squashtest.ta.core.tools.io.FileTree;
import org.squashtest.ta.maven.AbstractBaseSquashTaMojo;

/**
 * This object provides initialization procedures for Squash TF maven plugins designed 
 * to be able to work without a pom file.
 * 
 * @author edegenetais
 */
public class NoPomPluginInitializer {
    
    private static final String BASEDIR_EXPRESSION = "${project.basedir}";
    private static final String INITIALIZER_COMPONENT_NAME = NoPomPluginInitializer.class.getSimpleName();
    private Log log;
    
    private AbstractBaseSquashTaMojo targetMojo;

    /**
     * Creates the initializer instance.
     * @param targetMojo the mojo we want to initialize with this initializer.
     */
    public NoPomPluginInitializer(AbstractBaseSquashTaMojo targetMojo) {
        if(targetMojo==null){
            throw new IllegalArgumentException("The target mojo must NOT be null !");
        }else{
            log = targetMojo.getLog();
            if(log==null){
                log=new FallbackLogger();
                log.error("No log defined.");
            }
            if(log.isDebugEnabled()){
                log.debug("Creating "+INITIALIZER_COMPONENT_NAME+" for mojo "+targetMojo.getClass());
            }
        }
        this.targetMojo = targetMojo;
    }
    
    public void apply(File mojoPreinitOutputDirectory,File mojoPreinitBaseDir) throws IOException{
        if(log.isInfoEnabled()){
            log.info("Applying "+INITIALIZER_COMPONENT_NAME);
        }
        if(mojoPreinitOutputDirectory==null){
            final File outputDirectory = new File(mojoPreinitBaseDir,"target/squashTF");
            targetMojo.setOutputDirectory(outputDirectory);
            log.debug(INITIALIZER_COMPONENT_NAME+" has set output directory to "+outputDirectory.getAbsolutePath());
        }else{
            replaceBasedirInPathExpression(mojoPreinitOutputDirectory, mojoPreinitBaseDir);
        }
        
    }

    private void replaceBasedirInPathExpression(File mojoPreinitOutputDirectory, File mojoPreinitBaseDir) throws IOException {
        final String preinitPathExpression = mojoPreinitOutputDirectory.getPath();
        if(preinitPathExpression.contains(BASEDIR_EXPRESSION)){
            if(log.isDebugEnabled()){
                log.debug("Initial outputDirectory="+mojoPreinitOutputDirectory.getPath()+" InitialBaseDir="+mojoPreinitBaseDir);
            }
            File actualOutputDirectory;
            if (preinitPathExpression.startsWith(BASEDIR_EXPRESSION)) {//the property is used as a path prefix, we may just replacing it.
                String actualOutputPath = preinitPathExpression.replace(BASEDIR_EXPRESSION, mojoPreinitBaseDir.getPath());
                actualOutputDirectory = new File(actualOutputPath);
            } else if (basedirPropJustAfterFileSeparator(preinitPathExpression)) {
                /*
                * The property is used as a path segment, we need to check for the default maven init behavior without pom,
                * where baseDir and project.build.directory gets built as currentDir/${project.basedir}
                */
                File baseOfOutput = new File(preinitPathExpression.substring(0, preinitPathExpression.indexOf(BASEDIR_EXPRESSION)));
                String relativeBasePath = FileTree.FILE_TREE.getRelativePath(baseOfOutput, mojoPreinitBaseDir);
                if (relativeBasePath==null) {
                    /*
                    * If we had configured an absolute base dir outside of the project, define the outputdir based on that instead of relative to basedir.
                    * Otherwise we don't honor the configuration.
                    */
                    relativeBasePath = mojoPreinitBaseDir.getPath();
                }
                String actualOutputPath = preinitPathExpression.replace(BASEDIR_EXPRESSION, relativeBasePath);
                actualOutputDirectory = new File(actualOutputPath);
            } else {
                //well ... we don't knwo what the user wants with this, so let's dumbly substitute and warn
                if(log.isWarnEnabled()){
                    log.warn(BASEDIR_EXPRESSION+" cannot be construed as a base directory or path segment in "+preinitPathExpression+", falling back to pure substitution.");
                }
                final String basePath = mojoPreinitBaseDir.getPath();
                String actualOutputPath = preinitPathExpression.replace(BASEDIR_EXPRESSION, basePath);
                actualOutputDirectory = new File(actualOutputPath);
            }
            log.debug(INITIALIZER_COMPONENT_NAME+" has set output directory to "+actualOutputDirectory.getAbsolutePath());
            targetMojo.setOutputDirectory(actualOutputDirectory);
        }else{
            log.debug(INITIALIZER_COMPONENT_NAME+" has kept the initial outputDirectory ("+mojoPreinitOutputDirectory.getAbsolutePath()+")");
        }
    }

    /**
     * This method checks that the basedir property is inserted just after a file separator'.
     * Without this condition, we don't know if extracting a relative path is actually meaningfull.
     * 
     * @param preinitPathExpression the expression which we want to substitute.
     * @return <code>true</code> if basedir is inserted just after a file separator.
     */
    private static boolean basedirPropJustAfterFileSeparator(final String preinitPathExpression) {
        final int expressionOccurrenceBegin = preinitPathExpression.indexOf(BASEDIR_EXPRESSION); 
        final char charBeforePropertyOccurrence = preinitPathExpression.charAt(expressionOccurrenceBegin-1);
        return charBeforePropertyOccurrence == '/' ||
                charBeforePropertyOccurrence == File.separatorChar;
    }


}
