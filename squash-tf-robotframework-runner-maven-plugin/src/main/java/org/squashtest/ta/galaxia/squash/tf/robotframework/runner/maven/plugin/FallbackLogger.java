/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2019 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.squash.tf.robotframework.runner.maven.plugin;

import org.apache.maven.plugin.logging.Log;

/**
 *
 * @author edegenetais
 */
class FallbackLogger implements Log {

    @Override
    public boolean isDebugEnabled() {
        return false;
    }

    @Override
    public void debug(CharSequence cs) {
        debug(cs, null);
    }

    @Override
    public void debug(CharSequence cs, Throwable thrwbl) {
        /*Never gonna log debug*/
    }

    @Override
    public void debug(Throwable thrwbl) {
        debug("", thrwbl);
    }

    @Override
    public boolean isInfoEnabled() {
        return true;
    }

    @Override
    public void info(CharSequence cs) {
        System.err.println("[INFO]" + cs);
    }

    @Override
    public void info(CharSequence cs, Throwable thrwbl) {
        info(cs);
        info(thrwbl);
    }

    @Override
    public void info(Throwable thrwbl) {
        thrwbl.printStackTrace();
    }

    @Override
    public boolean isWarnEnabled() {
        return true;
    }

    @Override
    public void warn(CharSequence cs) {
        System.err.println("[WARN]" + cs);
    }

    @Override
    public void warn(CharSequence cs, Throwable thrwbl) {
        warn(cs);
        warn(thrwbl);
    }

    @Override
    public void warn(Throwable thrwbl) {
        thrwbl.printStackTrace();
    }

    @Override
    public boolean isErrorEnabled() {
        return true;
    }

    @Override
    public void error(CharSequence cs) {
        System.err.println("[ERROR]" + cs);
    }

    @Override
    public void error(CharSequence cs, Throwable thrwbl) {
        error(cs);
        error(thrwbl);
    }

    @Override
    public void error(Throwable thrwbl) {
        thrwbl.printStackTrace();
    }

}
