/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2019 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.squash.tf.robotframework.runner.maven.plugin;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.squashtest.ta.backbone.init.EngineLoader;
import org.squashtest.ta.commons.exporter.ReportType;
import org.squashtest.ta.commons.exporter.ResultExporter;
import org.squashtest.ta.commons.exporter.html.HtmlSummarySuiteResultExporter;
import org.squashtest.ta.commons.exporter.surefire.SurefireSuiteResultExporter;
import org.squashtest.ta.framework.facade.Engine;
import org.squashtest.ta.framework.facade.TFTestWorkspaceBrowser;
import org.squashtest.ta.framework.test.definition.TestSuite;
import org.squashtest.ta.framework.test.result.SuiteResult;
import org.squashtest.ta.galaxia.robotframework.runner.exploitation.explorer.RobotFrameworkTestExplorer;
import org.squashtest.ta.galaxia.robotframework.runner.exploitation.factory.RobotFrameworkTestSuiteFactory;
import org.squashtest.ta.galaxia.robotframework.runner.exploitation.factory.RobotFrameworkTestWorkspaceBrowser;
import org.squashtest.ta.galaxia.robotframework.runner.exploitation.link.RobotFrameworkTestEcosystemModel;
import org.squashtest.ta.galaxia.robotframework.runner.exploitation.link.RobotFrameworkTestSpecParser;
import org.squashtest.ta.galaxia.robotframework.runner.exploitation.link.TestPointer;
import org.squashtest.ta.galaxia.robotframework.runner.exploitation.link.TmTradeoffRobotFrameworkTestSpecParser;
import org.squashtest.ta.galaxia.robotframework.runner.exploitation.link.TmTradeoffTestPointer;
import org.squashtest.ta.galaxia.squash.tf.robotframework.runner.maven.json.listing.RobotFrameworkRunnerJsonTree;
import org.squashtest.ta.galaxia.squash.tf.robotframework.runner.maven.metadata.RobotFWRunnerMetadataSyntaxChecker;
import org.squashtest.ta.galaxia.tf.param.service.ParametersFactory;
import org.squashtest.ta.maven.AbstractSquashTaMojo;

/**
 * 
 * This goal runs one or more robotframeworek with added Squash TF context for execution in the Squash ecosystem.
 * Added functionalities include de Squash TM-Squash TF link for test result reporting.
 * 
 * @author edegenetais
 */
@Mojo(
        name = "run",
        defaultPhase = LifecyclePhase.INTEGRATION_TEST,
        requiresProject = false
)
public class SquashTFRobotframeworkRunnerMojo extends AbstractSquashTaMojo{
    
    private static final String TEST_SUITE="tf.test.suite";
    private static final String TM_TRADEOFF_TEST_SUITE = "tf.robot.testSuite";
    
    @Parameter(
            property=TEST_SUITE
    )
    private String testList;

    @Parameter(
            property = TM_TRADEOFF_TEST_SUITE,
            name = "tfRobotTestSuite"
    )
    private String tmTradeoffTestList;
    
    @Parameter(
            property = "tf.test.dir"
    )
    private File testDir;
    
    public String getTestList() {
        return testList;
    }

    public void setTestList(String testList) {
        this.testList = testList;
    }

    public String getTfRobotTestSuite() {
        return tmTradeoffTestList;
    }

    public void setTfRobotTestSuite(String tfRobotTestSuite) {
        this.tmTradeoffTestList = tfRobotTestSuite;
    }

    public File getTestDir() {
        return testDir;
    }

    public void setTestDir(File testDir) {
        this.testDir = testDir;
    }
    
    @Override
    protected SuiteResult executeImpl() throws MojoExecutionException, MojoFailureException {
        
        getLogger().info("Initialization");           
        initLogging();
        init();
        
        TestSuite suite;
        
        if(this.testList !=null && this.tmTradeoffTestList !=null){
            throw new MojoExecutionException("You may not set both "+TEST_SUITE+" and "+TM_TRADEOFF_TEST_SUITE);
        }
        
        ParametersFactory parmFactory;
        if(this.tmTradeoffTestList!=null && !this.tmTradeoffTestList.isEmpty()){
            // TM warp mode preempts the (default) normal mode.
            TmTradeoffRobotFrameworkTestSpecParser testSpecParser=new TmTradeoffRobotFrameworkTestSpecParser(this.testDir, new RobotFrameworkTestExplorer(),getBaseDir(),tmTradeoffTestList);
            List<TmTradeoffTestPointer> testPointers=testSpecParser.parseTestList();
            
            getLogger().debug(testPointers.size()+" tests found.");
                        
            parmFactory=new ParametersFactory(testSpecParser.parseTestSuite());
            suite=new RobotFrameworkTestSuiteFactory().buildSuite(testPointers,parmFactory);
            
            getLogger().info("Squash TF: Metadata are not taken into account in TM TF Link");
                       
        }else{
            RobotFrameworkTestSpecParser testSpecParser=new RobotFrameworkTestSpecParser(this.getBaseDir(), testList);
            List<TestPointer> testPointers=testSpecParser.parseTestList();
            
            getLogger().debug(testPointers.size()+" tests found.");
                        
            parmFactory=new ParametersFactory(testSpecParser.parseTestSuite());
            suite=new RobotFrameworkTestSuiteFactory().buildSuite(testPointers,parmFactory);
                      
            //Cheking Metadata
            RobotFrameworkTestExplorer explorer = new RobotFrameworkTestExplorer();
            File projectBase=getBaseDir();
            RobotFrameworkRunnerJsonTree robotFWRunnerJsonTree = new RobotFrameworkRunnerJsonTree(getOutputDirectory(), explorer, projectBase);   
            
            Map<String, List<TestPointer>> ecosystemGroups = new RobotFrameworkTestEcosystemModel(testPointers).groupPointersByEcosystemDefinition();
            
            getLogger().info("Squash TF: checking metadata...");
             
            new RobotFWRunnerMetadataSyntaxChecker(robotFWRunnerJsonTree,ecosystemGroups, false, false, false).checkMetadataSyntaxByDefault();
            
            getLogger().info("Squash TF: Metadata checking successfully completed");
        }
        Properties globalParms;
        if(parmFactory.getParameters()==null){
            globalParms=new Properties();
        }else{
            globalParms=parmFactory.getParameters().getGlobalParamsList();
        }
        TFTestWorkspaceBrowser browser=new RobotFrameworkTestWorkspaceBrowser(globalParms,this.getBaseDir());
              
        Engine e=new EngineLoader().load();
        applyConfigurers(e);

        return e.execute(suite, browser);
    }

    private void init() throws MojoExecutionException{
        
        try {
            if(getExporters().length==1 && getExporters()[0] instanceof SurefireSuiteResultExporter){
                setExporters(new ResultExporter[]{new HtmlSummarySuiteResultExporter()});
            }
            
            new NoPomPluginInitializer(this).apply(getOutputDirectory(), getBaseDir());
            
            if(testList==null && tmTradeoffTestList==null) {
                testList = "**/*";
            }
            
        } catch (IOException ex) {
            throw new MojoExecutionException("Failure during mojo initialization. Configuration is probably wrong.",ex);
        }
    }
    
    @Override
    protected ReportType getReportType() {
        return ReportType.EXECUTION;
    }


}
