/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2019 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.squash.tf.robotframework.runner.maven.json.listing;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import org.junit.Test;
import org.junit.Before;
import org.squashtest.ta.api.test.toolkit.ResourceExtractorTestBase;
import org.squashtest.ta.galaxia.robotframework.runner.exploitation.explorer.RobotFrameworkTestExplorer;
import org.squashtest.ta.galaxia.robotframework.runner.exploitation.link.TestPointer;

/**
 * Test of the RFW specific part. 
 * Most of the Json codec gets tested in the addon toolkit, so just do one test here to check
 * that the specific codec implementation just works with a basic dataset.
 * 
 * @author edegenetais
 */
public class RobotFrameworkRunnerJsonTreeTest extends ResourceExtractorTestBase{
    
    private File destinationDir;
    private RobotFrameworkRunnerJsonTree testee;
    private RobotFrameworkTestExplorer explorer= new RobotFrameworkTestExplorer();
    private File projectBase;
    
    @Before
    public void destinationDirNtestee() throws IOException{
        destinationDir=createNtrackDir();
       
        projectBase= new File("test");
        testee=new RobotFrameworkRunnerJsonTree(destinationDir,explorer,projectBase);
        testee.setDateFactory(new RobotFrameworkRunnerJsonTree.DateFactory() {
            @Override
            public Date getCurrentDate() {
                return new Date(0);
            }
        });
    }
    
    @Test
    public void checkThatTypicalDataGiveTypicalResult() throws IOException{
        Map<String,List<TestPointer>> testPointersEcosystem=new HashMap<>();
        
        putEcosystemEntryInMap(testPointersEcosystem, "Keyword Driven", "keyword_driven.robot", "Test that works", "Test that fails");
        putEcosystemEntryInMap(testPointersEcosystem, "Broken Test Suite", "broken_keyword_driven.robot", "Tah one fails","That one misses a keyword");
        
        testee.write(testPointersEcosystem, false);
        
        final File expectedJson = createFile("expected-listing.json");
        checkActualContentAgainstExpected(new File(destinationDir,"test-tree/testTree.json"), expectedJson);
    }

    private void putEcosystemEntryInMap(Map<String, List<TestPointer>> testPointersEcosystem, String suiteName, String suitePath, String... testNames) {
        
        final List<TestPointer> testPointerList = new ArrayList<>();
        for(String testName:testNames){
            testPointerList.add(new TestPointer(new File(suitePath), suiteName, testName));
        }
        testPointersEcosystem.put(suiteName, testPointerList);
    }
}
