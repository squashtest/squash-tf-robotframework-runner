/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2019 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.squash.tf.robotframework.runner.maven.plugin;

import org.apache.maven.plugin.logging.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author edegenetais
 */
class TestLoggerMock implements Log {

    public TestLoggerMock() {
    }
    private final Logger logger = LoggerFactory.getLogger(NoPomPluginInitializerTest.class.getName() + "$DrivenMojoMock");

    @Override
    public boolean isDebugEnabled() {
        return logger.isDebugEnabled();
    }

    @Override
    public void debug(CharSequence cs) {
        logger.debug(cs.toString());
    }

    @Override
    public void debug(CharSequence cs, Throwable thrwbl) {
        logger.debug(cs.toString(), thrwbl);
    }

    @Override
    public void debug(Throwable thrwbl) {
        logger.debug("A penguin hater logged an exception without context message. That is BAD !", thrwbl);
    }

    @Override
    public boolean isInfoEnabled() {
        return logger.isInfoEnabled();
    }

    @Override
    public void info(CharSequence cs) {
        logger.info(cs.toString());
    }

    @Override
    public void info(CharSequence cs, Throwable thrwbl) {
        logger.info(cs.toString(), thrwbl);
    }

    @Override
    public void info(Throwable thrwbl) {
        logger.info("A penguin hater logged an exception without context message. That is BAD !", thrwbl);
    }

    @Override
    public boolean isWarnEnabled() {
        return logger.isWarnEnabled();
    }

    @Override
    public void warn(CharSequence cs) {
        logger.warn(cs.toString());
    }

    @Override
    public void warn(CharSequence cs, Throwable thrwbl) {
        logger.warn(cs.toString(), thrwbl);
    }

    @Override
    public void warn(Throwable thrwbl) {
        logger.warn("A penguin hater logged an exception without context message. That is BAD !", thrwbl);
    }

    @Override
    public boolean isErrorEnabled() {
        return logger.isInfoEnabled();
    }

    @Override
    public void error(CharSequence cs) {
        logger.error(cs.toString());
    }

    @Override
    public void error(CharSequence cs, Throwable thrwbl) {
        logger.error(cs.toString(), thrwbl);
    }

    @Override
    public void error(Throwable thrwbl) {
        logger.error("A penguin hater logged an exception without context message. That is BAD !", thrwbl);
    }

}
