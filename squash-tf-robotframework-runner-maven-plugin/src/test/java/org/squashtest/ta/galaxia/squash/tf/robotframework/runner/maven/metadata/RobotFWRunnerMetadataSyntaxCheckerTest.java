/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2019 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.squash.tf.robotframework.runner.maven.metadata;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.squashtest.ta.commons.factories.TestSyntaxException;
import org.squashtest.ta.galaxia.robotframework.runner.exploitation.link.TestPointer;
import org.squashtest.ta.galaxia.squash.tf.robotframework.runner.maven.json.listing.RobotFrameworkRunnerJsonTree;
import org.squashtest.ta.plugin.robot.fw.library.RobotFWTestAnomaly;

/**
 *
 * @author dclaerhout
 */
public class RobotFWRunnerMetadataSyntaxCheckerTest {
    
 
    private final Map<String, List<TestPointer>> ecosystemGroups = new HashMap<>();
    private List<TestPointer> ecosystemGroup = new ArrayList<>();
    private TestPointer ecoTest;
    private TestPointer anotherEcoTest;
    private RobotFrameworkRunnerJsonTree robotFWRunnerJsonTree;
    Map<String, List<String>> metadataList = new HashMap<>();
    List<String> metadataValues;
    private List<RobotFWTestAnomaly> testAnomalies = new ArrayList<>();
    private RobotFWTestAnomaly testAnomaly;
    
    
    @Before
    public void setup() {
        
    ecoTest = mock(TestPointer.class);
    when(ecoTest.getSuiteName()).thenReturn("testSuite");
    when(ecoTest.getTestName()).thenReturn("testName");
    
    anotherEcoTest = mock(TestPointer.class);
    when(anotherEcoTest.getSuiteName()).thenReturn("testSuite2");
    when(anotherEcoTest.getTestName()).thenReturn("testName2");    
    
    ecosystemGroup.add(ecoTest);
    ecosystemGroup.add(anotherEcoTest);    
    ecosystemGroups.put("firstEcosystemGroup", ecosystemGroup);
    
    metadataValues=  new ArrayList<>();

    robotFWRunnerJsonTree = mock(RobotFrameworkRunnerJsonTree.class);
  
    when(robotFWRunnerJsonTree.getMetadataForTestPointer(ecoTest)).thenReturn(metadataList);
    when(robotFWRunnerJsonTree.getRobotFWTestAnomalies(ecoTest)).thenReturn(testAnomalies);
    }
    
    @Test
    public void noAnomalyInRobotFWTest() {
                       
        RobotFWRunnerMetadataSyntaxChecker testee = new RobotFWRunnerMetadataSyntaxChecker(robotFWRunnerJsonTree, ecosystemGroups, true, true, false);
        
        metadataValues.add("oneValue");
        metadataList.put("MetadataKey", metadataValues);
         
        testee.checkMetadataSyntaxByDefault();
    }    
    
 
    @Test(expected = TestSyntaxException.class)
    public void anomaliesInRobotFWTest() {
        
        testAnomaly = mock(RobotFWTestAnomaly.class);
        when(testAnomaly.getType()).thenReturn("Duplicate_key");
        when(testAnomaly.getMessage()).thenReturn("duplicateKeyValue");    
        testAnomalies.add(testAnomaly);
                
        RobotFWRunnerMetadataSyntaxChecker testee = new RobotFWRunnerMetadataSyntaxChecker(robotFWRunnerJsonTree, ecosystemGroups, true, true, false);

        metadataValues.add("oneValue");
        metadataList.put("MetadataKey", metadataValues);
        
        testee.checkMetadataSyntaxByDefault();
    }

    @Test(expected = TestSyntaxException.class)
    public void metadataHasSyntaxError() {
        
        metadataValues.add("&é###@");
        metadataList.put("metadataKey2", metadataValues);   
                
        RobotFWRunnerMetadataSyntaxChecker testee = new RobotFWRunnerMetadataSyntaxChecker(robotFWRunnerJsonTree, ecosystemGroups, true, true, false);
        
        testee.checkMetadataSyntaxByDefault();
    }
    
    @Test(expected = TestSyntaxException.class)
    public void metadataValuesAreNotUniqueAcrossEcosystems() {

        metadataValues.add("oneValue");
        metadataList.put("metadataKey", metadataValues);
        
        when(robotFWRunnerJsonTree.getMetadataForTestPointer(anotherEcoTest)).thenReturn(metadataList);

        RobotFWRunnerMetadataSyntaxChecker testee = new RobotFWRunnerMetadataSyntaxChecker(robotFWRunnerJsonTree, ecosystemGroups, true, true, true);
        
        testee.checkMetadataSyntaxWithUnicityChecking(null);
    }
}
