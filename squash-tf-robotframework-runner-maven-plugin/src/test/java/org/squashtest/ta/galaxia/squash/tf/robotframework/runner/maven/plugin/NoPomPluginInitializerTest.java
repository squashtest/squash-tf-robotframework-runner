/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2019 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.squash.tf.robotframework.runner.maven.plugin;

import java.io.File;
import java.io.IOException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.squashtest.ta.api.test.toolkit.ResourceExtractorTestBase;
import org.squashtest.ta.maven.AbstractSquashTaMojo;

/**
 *
 * @author edegenetais
 */
public class NoPomPluginInitializerTest extends ResourceExtractorTestBase{
    
    private NoPomPluginInitializer testee;
    private AbstractSquashTaMojo drivenMojoMock;
    private File projectBase;
    
    @Before
    public void setUp() throws IOException{
        drivenMojoMock=Mockito.mock(AbstractSquashTaMojo.class);
        Mockito.when(drivenMojoMock.getLog()).thenReturn(new TestLoggerMock());
        testee=new NoPomPluginInitializer(drivenMojoMock);
        projectBase=createNtrackDir();
    }
    
    /**
     * This test case is met in the default case where we use one of the mojos without pom 
     * and without overriding any system property, with current working directory 
     * in the robotframework directory, which is an important use case because that how we'll want to run most of the time.
     * @throws IOException 
     */
    @Test
    public void testBasedirInBaseDir() throws IOException{
        File outputDir=new File(projectBase,"${project.basedir}/target/squashTF");
        File expectedResolvedOutputDir=new File(projectBase,"target/squashTF");
        testee.apply(outputDir, projectBase);
        Mockito.verify(drivenMojoMock).setOutputDirectory(expectedResolvedOutputDir);
    }
    
    /**
     * This test case may be met if we want to override report location by specifying a
     * relative path based on project.basedir
     * @throws IOException 
     */
    @Test
    public void testOutputRelativeToPropertyBaseDir() throws IOException{
        File outputDir=new File("${project.basedir}/target/squashTF");
        File expectedResolvedOutputDir=new File(projectBase,"target/squashTF");
        testee.apply(outputDir, projectBase);
        Mockito.verify(drivenMojoMock).setOutputDirectory(expectedResolvedOutputDir);
    }
    
    @Test
    public void testAbsoluteOutputOutsideOfProject() throws IOException{
        final String absoluteOtherBasePath = createNtrackDir().getAbsolutePath();
        File outputDir=new File(absoluteOtherBasePath+"/${project.basedir}/target/squashTF");
        File expectedResolvedOutputDir=new File(absoluteOtherBasePath,projectBase.getPath()+"/target/squashTF");
        testee.apply(outputDir, projectBase);
        Mockito.verify(drivenMojoMock).setOutputDirectory(expectedResolvedOutputDir);
    }
    
    @Test
    public void donTchangeAnythingIfNoPropOccurrence() throws IOException{
        File outputDir=new File("/this/Is/Path/target/squashTF");
        testee.apply(outputDir, projectBase);
        Mockito.verify(drivenMojoMock,Mockito.never()).setOutputDirectory(Mockito.any(File.class));
    }

}
