#
#     This file is part of the Squashtest platform.
#     Copyright (C) 2019 - 2020 Henix
#
#     See the NOTICE file distributed with this work for additional
#     information regarding copyright ownership.
#
#     This is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Lesser General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     this software is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Lesser General Public License for more details.
#
#     You should have received a copy of the GNU Lesser General Public License
#     along with this software.  If not, see <http://www.gnu.org/licenses />.
#

from setuptools import setup, find_packages

setup (
       name='squash_tf_services',
       version='1.0.0',
       packages=find_packages(),

       # Declare your packages' dependencies here, for eg:
       install_requires=[],

       # Fill in these to make your Egg ready for upload to
       # PyPI
       author='Henix',
       author_email='faq-python@squashtest.com',
       keywords='automated_testing robotframework SquashTM Squash TF',
       description = 'This package allows to use test parameters from Squash directly in tests.',
       url='https://squash-tf.readthedocs.io/projects/runner-robotframework/en/doc-stable/tf-param-service.html',
       license='License :: OSI Approved :: GNU Lesser General Public License v3 (LGPLv3)',
       long_description='This package gives acces to parameters transmitted from Squash TM when '+
       'issuing the test suite execution order.',

       # could also include long_description, download_url, classifiers, etc.
       classifiers=[
          "Programming Language :: Python :: 2.7",
          "Programming Language :: Python :: 3",
          "License :: OSI Approved :: GNU Lesser General Public License v3 (LGPLv3)"
       ]
)
