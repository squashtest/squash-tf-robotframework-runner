..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2019 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

################
TF Param Service
################

This component provided by the ``squash-tf-services`` library allows you to retrieve the values 
of the parameters transmitted to the runner through the **json** test suite file 
(typically provided by |squashTM|) in order to use them in your robotframework tests.
All keywords defined by the library allow the caller to define default values in the test, 
so that it may be run outside of the runner.

*************
Configuration
*************

In order to use the TFParamService keywords in your test suite, you have to
    *   install the python library. We assume here that the pip python package manager is installed.

        .. code-block:: bash

           python -m pip install squash-tf-services

        Make sure that this command has been executed in all environement where the tests will be run,
        in the relevant python environement. If you use a symlink and path setting in a linux environment
        to use python3 as your 'python' binary, make sure this setting is in effect when you run the install
        command.
        
    *   Import the keyword library by using the following Library statement in your test :
    
        .. code-block:: python

           Library squash_tf.TFParamService

************************************
Using parameter values in your tests
************************************

To retrieve parameter values in your tests, use one of the three defined keywords in the following way :

.. code-block:: python

   ${value}= Get Param    <key>    [<optional_default_value>]

You may omit the optional default value if ``None`` is manageable in your test. 
Otherwise, the second parameter will define a default value used if no parameter value is
defined for your key. This will happen if :

*   The test is executed outside of the runner
*   There is no parameter value for the key you have used 
    (for exemple, no value for this test case)


=================
Known limitations
=================

If you use python2, encoding will not be properly managed in parameter values.
This problem does not exist if you run the same tests using python3. As python2 will reached
its end of life on next january (official EOL date 2020/01/01), we recommend using python3.

********
Keywords
********
    *   
        +-----------+------------------+------+---------------+
        | ${value}= | Get Global Param |  key |               |
        +-----------+------------------+------+---------------+
        | ${value}= | Get Global Param |  key | default_value |
        +-----------+------------------+------+---------------+

        This keyword retrieves the value of a parameter defined for all tests executed in this run.
        If there is a global value mapped on this key, the ``value`` argument is assigned with it,
        otherwise it is assigned with the default value. If no default value was given, ``value``
        becomes ``None``.
        Ex\ |_|:

        .. code-block:: python
           
           ${parameter}=    Get Global Param    target_base_url    http:localhost:8080/sut/


    *   
        +-----------+----------------+-----+---------------+
        | ${value}= | Get Test Param | key |               |
        +-----------+----------------+-----+---------------+
        | ${value}= | Get Test Param | key | default_value |
        +-----------+----------------+-----+---------------+

        This keyword retrieves the value of a parameter defined for one execution of a test case
        in this run.
        This means that the run, as built through the json test suite file build by Squash TM,
        may schedule several execution of the same test case with different value for the parameter.
        If there is a value mapped on this key for the specific test case run, 
        the ``value`` argument is assigned with it,otherwise it is assigned with the default value. 
        If no default value was given, ``value`` becomes ``None``.
        Ex\ |_|:

        .. code-block:: python
           
           ${login}=    Get Test Param    TC_login    test_user


    *   
        +-----------+-----------+-----+---------------+
        | ${value}= | Get Param | key |               |
        +-----------+-----------+-----+---------------+
        | ${value}= | Get Param | key | default_value |
        +-----------+-----------+-----+---------------+

        This keyword combines both scopes. If a parameter value is mapped on this key
        for this test case run, then the ``value`` argument is assigned with this value. 
        If not, global parameters are queried. If a global parameter value is mapped on the given key, 
        then the ``value`` argument is assigned with this value. Otherwise, ``value`` becomes ``None``.
        Ex\ |_|:

        .. code-block:: python
           
           ${login}=    Get Param    login    test_user

*******************************
Manually providing a .json file
*******************************

If you want to manually provide a **.json** file, you need to add the following parameter on your runner command line
**-Dtf.test.suite={file:path/to/json/FileName.json}**.

**"path/to/json/FileName.json"** must be the relative path of your **.json file** from the root of your project.

If the **.json file** is located directly at the root of your project, just type **-Dtf.test.suite={file:FileName.json}**

**For example :**

.. code-block::  bash

   mvn org.squashtest.ta.galaxia:squash-tf-robotframework-runner-maven-plugin:1.0.0-RELEASE:run -Dtf.test.suite={file:testSuite.json}

.. code-block:: json

    {
        "test": [{
                "id": "39",
                "script": "My Test Suite/First Test Case",
                "param": {
                    "TC_REFERENCE": "",
                    "TC_CUF_CUF_CUSTOM": "true",
                    "TC_UUID": "3a7099ff-ab59-4e99-b21d-07e7d71d1ed5"
                }
            }, {
                "id": "40",
                "script": "My Test Suite/Second Test Case",
                "param": {
                    "TC_REFERENCE": "",
                    "DS_name": "Bertrand",
                    "DSNAME": "dataset1",
                    "TC_CUF_CUF_CUSTOM": "true",
                    "DS_age": "41",
                    "TC_UUID": "adec6164-5dec-4c8c-a0ae-d836370d519b"
                }
            }, {
                "id": "41",
                "script": "My Test Suite/Second Test Case",
                "param": {
                    "TC_REFERENCE": "",
                    "DS_name": "Damien",
                    "DSNAME": "dataset2",
                    "TC_CUF_CUF_CUSTOM": "true",
                    "DS_age": "undefined",
                    "TC_UUID": "adec6164-5dec-4c8c-a0ae-a036bf0d519b"
                }
            }],
        "param": {
            "globalParamSection": "This is global param section",
            "user": "foo",
            "ow_ner.Na-me": "bar",
        }
    }       
    

