..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2019 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

#############################
Robot Framework tests running
#############################

This Mojo enables one to run a selection of, or all possible, Robot Framework  tests and report
their execution. In order to do so one only needs to run the following command :

.. code-block:: shell

  mvn org.squashtest.ta.galaxia:squash-tf-robotframework-runner-maven-plugin:1.0.0-RELEASE:run

* ``mvn`` : launch Maven
* ``org.squashtest.ta.galaxia:squash-tf-robotframework-runner-maven-plugin:1.0.0-RELEASE:run``  :
  the actual running Mojo provided by |squashTF| Robot Framework Runner.


By default the whole collection of tests available in the project will be executed.
A summary of the execution is reported and available at
``target/squashTF/html-reports/squash-tf-report.html``.

If one wants to only run a subset of possible test one can provide a list of :

* tests cases with the maven property "tf.test.suite"

  .. code-block:: shell

     mvn org.squashtest.ta.galaxia:squash-tf-robotframework-runner-maven-plugin:1.0.0-RELEASE:run -Dtf.test.suite="MyTest/Ihm/Add data;MyTest/Rest/Clean data"

  .. warning::

     Be careful, the full qualified test case name is case sensitive. To prevent problems use the names provided by the ``list`` goal

* tests suites (.robot file) with the maven property "tf.robot.testSuite"

  .. code-block:: shell

     mvn org.squashtest.ta.galaxia:squash-tf-robotframework-runner-maven-plugin:1.0.0-RELEASE:run -Dtf.robot.testSuite="myTest/ihm.robot;myTest/rest.robot"

.. note::

   These two properties are mutually exclusive. You can only use one of them in a command line.

For both of this maven property two mechanism are possible:

* Mimic TM-TF link and provide a list of selected test via a Json file. In this
  scenario the parameter should be given the value ``{file:testsuite.json}``
  and the testsuite.json file should be put at the root of the project.
* Provide a CSV like line, where qualified tests names are listed separated by semicolons

.. warning::
   If there are metadata syntax errors in the running test script(s), **warning** message(s) will be displayed in the console.
   (See :ref:`metadata-in-robot-framework-runner` for more information about Metadata syntax conventions)
