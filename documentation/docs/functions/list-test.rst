..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2019 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

######################################
List implemented Robot Framework tests
######################################

This Mojo enables one to list as a Json file the available implemented tests. In
order to do so one only needs to run the following command :

.. code-block:: shell

  mvn org.squashtest.ta.galaxia:squash-tf-robotframework-runner-maven-plugin:1.0.0-RELEASE:list

The command is structured as follows

* ``mvn`` : launch Maven
* ``org.squashtest.ta.galaxia:squash-tf-robotframework-runner-maven-plugin:1.0.0-RELEASE:list``  :
  the actual listing Mojo provided by the |squashTF| Robot Framework Runner. It lists
  all Robot Framework test present in the project.

.. note::

  This listing doesn't list only ".robot" file but also tests cases inside the .robot file.

*************************
'list' goal with Metadata
*************************

If there are Metadata in the current test project, the goal *'list'* searches and checks if all metadata respect the conventions for writing and using Squash TF metadata 
(See :ref:`metadata-in-robot-framework-runner` for more information about Metadata syntax conventions).

The goal will check through the project and collect all the metadata error(s).

If there are any error, the build will fail and a list of the incorrect metadata will be displayed. Otherwise, a **SUCCESS** result will be obtained.

The test suite and test case names are also displayed with the incorrect metadata.

.. image:: ./../_static/images/metadata/metadata-error-list-example.png
   :align: center

|

**************************************
Listing test JSON report with Metadata
**************************************

If the build is successful, the generated report (JSON file) will contain the metadata associated with each of the test scripts.

.. code-block:: json

    {
        "timestamp": "2019-12-11T16:45:40.259+0000",
        "name": "tests",
        "contents": [
            {
                "name": "Main",
                "contents": [
                    {
                        "name": "Standard Case",
                        "metadata": {
                            "TC_UID": [
                                "598621846"
                            ],
                            "linked-TC": [
                                "444555552",
                                "465454603",
                                "481289439"
                            ],
                            "metadataWithOneValue": [
                                "12345"
                            ]
                        },
                        "contents": null
                    },
                    {
                        "name": "Special Char",
                        "metadata": {},
                        "contents": null
                    },
                    {
                        "name": "No Last Name",
                        "metadata": {},
                        "contents": null
                    },
                    {
                        "name": "No First Name",
                        "metadata": {},
                        "contents": null
                    }
                ]
            }
        ]
    }

.. note::

   To ignore thoroughly Metadata during the listing process as well as in the report, insert :guilabel:`tf.disableMetadata` property after the goal "list".

   .. code-block:: shell

     mvn org.squashtest.ta.galaxia:squash-tf-robotframework-runner-maven-plugin:1.0.0-RELEASE:list -Dtf.disableMetadata=true
