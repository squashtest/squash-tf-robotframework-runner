..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2019 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

################################
Squash TF Robot Framework Runner
################################

.. toctree::
   :hidden:
   :maxdepth: 2

   <Squash TF Doc Homepage> <https://squash-tf.readthedocs.io>
   Runner functions <functions/runner-functions.rst>
   Metadata in Robot Framework Runner <metadata.rst>
   TF Param Service <tf-param-service.rst>

========
Overview
========

The **Squash T**\ est **F**\ actory (|squashTF|) Robot Framework Runner aims to
provide a seamless integration to our ecosystem when automated test are implemented
using Robot Framework as the underlying test framework.

As a |squashTF|  runner its main goal is twofold :

* List in JavaScript Object Notation (Json) format the available implemented tests
* Run a selection (that can include all available tests) and report the execution.
  In the case where the execution order originates from **Squash T**\ est
  **M**\ anagement (|squashTM|), test status and report are sent back to
  |squashTM|.

=============
Prerequisites
=============

For the Squash TF Robot Framework runner to work, you should have installed on your execution environment 
(in addition to the `recommendation applicable to all Squash TF runners <https://squash-tf.readthedocs.io/en/latest/runner/overview.html#environment-configuration)>`_) :

* Python 2 or 3 (the "python" command should be in your be in your path) 
  To use the runner python3 under linux, you need to create a symlink named python that points to the to the python3 executable
  and put the directory where this link is located at the beginning of the PATH variable in your execution environement.
  In jenkins, this can be done in the dedicated node configuration.
* Robot Framework (the version you want)
* all robot external libraries requisite by your test project

.. note::

  The command :

  .. code-block:: shell

    python -m robot --version

  should succeed in order to Squash TF Robot Framework runner works.