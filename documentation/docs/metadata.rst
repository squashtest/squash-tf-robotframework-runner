..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2019 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. _metadata-in-robot-framework-runner:

##################################
Metadata in Robot Framework Runner
##################################

In your **.robot** file, you can insert Squash Metadata into test cases via tags.

**For example:**

.. image:: ./_static/images/metadata/metadata_example_in_robot_script.png
   :align: center

***************************
Metadata syntax conventions
***************************

In order to work properly, the following syntax rules have to be followed.

Spaces
------

In a Robot script, all tags must at least have **two spaces** between each other to be considered as a new tag. If you put only one space, it will be considered as the same tag.

.. image:: ./_static/images/metadata/space-between-tags.png


Prefix
------

To be recognized as a metadata, you have to prefixed it with **"tf:"**.

.. image:: ./_static/images/metadata/metadata-prefixe.png


Metadata Key
------------

In a Metadata annotation, the **key** is mandatory. A metadata key MUST be ONE WORD which contains only *alphanumeric characters, dashes, underscores and dots*.

.. image:: ./_static/images/metadata/metadata-key-syntax.png


.. important::
    Metadata key is **case insensitive** and must be **unique** in a test case.

    The same metadata key can be found in different test cases **but values can't be the same**. This is not a problem if the metadata key has no value.

Metadata Value
--------------

The **value** is optional. A metadata value MUST be ONE WORD which contains only *alphanumeric characters, dashes, slashes, underscores and dots*.

You must separate the metada key and value with an **"="** symbol.

.. image:: ./_static/images/metadata/metadata-value-syntax.png


.. important::
    Metadata value is **case sensitive** and must be assigned to a metadata key.

    If you duplicate two tags with exactly the same key and value, Robot Framework will automatically ignore the second tag with the same value.

    Furthermore, if you put two metadata **in the same test case** with the same key but with different values, our runner will inform you that there is a duplicate key.


Metadata with multiple values
-----------------------------

It is possible to have many metadata values associated to a same key.

To separate the different values, you must use a "|" symbol (Press AltGr and 6).

.. image:: ./_static/images/metadata/metadata-with-multiple-values.png


|

--------------------------------

*********************************
Use metadata for TM - TF autolink
*********************************

TF metadata handles the TM - TF autolink. Autolink is a feature to ease the link between a TM test case and a test automation script.
On TM side, a UUID is now provided (when the workflow is activated):

.. image:: ./_static/images/metadata/autolink-tm-uuid.png
   :align: center

This UUID is used as an identifier.

In your automation test, add a TF Metadata which key is ``linked-TC`` and value is the UUID from the corresponding TM test case.
As you can see in the example below, it's possible to link many TM test cases to the same automation test (two UUID are set in the "value" separated by a "|"):

.. image:: ./_static/images/metadata/linked-TC.png
   :align: center
