..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2019 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

################################################
Documents de conception du runner RobotFramework
################################################

La conception du runner doit permettre de répondre aux questions suivantes:


   1. `Comment intégrer et déployer RobotFramework <Intégration et déploiment de RobotFramework_>`_
   #. `Comment énumérer et référencer les tests <Énumération et référencement des tests_>`_
   #. `Comment piloter l'exécution des tests <Pilotage de l'exécution des tests_>`_, c'est-à-dire:

      1) Comment déployer correctement les tests

         a. s'ils sont autoporteurs
         #. s'ils ont des dépendances python tierces (cette partie pourra faire l'objet d'un lotissement)

         
      #) Comment lancer les tests en respectant l'ordre d'exécution issu de l'appelant au format Json Squash
      #) Comment récupérer et analyser les résultats


*******************************************
Intégration et déploiment de RobotFramework
*******************************************

Dans le cadre du Runner et de la TestFactorey, nous souhaitons limiter les prérequis nécessaires au niveau des hôtes d'excutions.
Pour cette raison, la première option considérée est le packaging sous forme de jar:

   * Sous cette forme, il n'est pas nécessaire d'installer python, du fait que l'exécution est prise en charge à travers jython (donc par la JVM).
   * La distribution dans les différents environnements de test (poste, master TF, agents) est assurée par maven (jar mavenisé) de manière homogène avec le 
     reste de l'écosystème Squash.


**************************************
Énumération et référencement des tests
**************************************

L'option la plus simple semble être un dry-run, qui effectue en plus la vérification de l'existence des mots-clefs, mais a l'intérêt de passer par
le format de sortie XML facile à analyser. Les autres points d'entrée trouvés sont des points de documentation destinés à l'Homme, donc le parsing
serait probablement plus fragile.

*********************************
Pilotage de l'exécution des tests
*********************************

--------------------
Déploiment des tests
--------------------
Par défaut les tests eux-mêmes sont extraits d'un SCM, ce qui porte leur déploiment.

.........................
Cas de tests autoporteurs
.........................
Le checkout des tests suffit.

....................................................................................................
Cas des tests utilisant des librairies tierces (mots clefs RobotFramework, fonctions/classes python)
....................................................................................................

à étudier : suggestion d'un lot ultérieur.

*******************
Exécution des tests
*******************

---------------------------------------------
Un plugin spécialisé sera créé au sein du SKF
---------------------------------------------

+----------+--------------------------------------------------------------------------------------------+---+
|Type      | Composant(s)                                                                               |Lot|
+==========+============================================================================================+===+
|Keywords  | Get result for RobotFramework test {testname} from suite {suiteName} [#added4Phase1Proto]_ | 1 |
|          |     of {bundle} as {result}                                                                |   |
|          +--------------------------------------------------------------------------------------------+   |
|          | Assert RobotFramework result {result} is passed                                            |   |
|          +--------------------------------------------------------------------------------------------+   |
|          | Launch RobotFramework {testname} from suite {suiteName} [#added4Phase1Proto]_              |   | 
|          |     bundle {bundle} and check [#isCompound]_                                               |   |
+----------+--------------------------------------------------------------------------------------------+---+
|Resources | bundle.robotFW                                                                             | 1 |
|          +--------------------------------------------------------------------------------------------+   |
|          | result.robotFW                                                                             |   |
+----------+--------------------------------------------------------------------------------------------+---+
|Converters| {file} to {bundle.robotFW}                                                                 | 1 |
+----------+--------------------------------------------------------------------------------------------+---+
|Commands  | execute WITH {bundle.robotFW} ON {void} USING {file} [#cfgExecFW]_                         |   |
|          |     AS {result.robotFW}                                                                    | 1 |
+----------+--------------------------------------------------------------------------------------------+---+
|Assertions| {result.robotFW} is success                                                                | 1 |
+----------+--------------------------------------------------------------------------------------------+---+

----------------------------------------------------------------------
Un runner spécialisé sera écrit pour intégrer le projet RobotFramework
----------------------------------------------------------------------

Le runner sera basé sur les principaux modules suivants :

.....................
Couche d'exploitation
.....................


1. En lot 1, ce composant se basera sur un dry-run pour analyser la structure du projet (sur la base du fichier xml produit par le dry-run).
   Sur la base de cette analyse initiale, il sera possible de produire :

   * Soit une liste de ``TestPointer`` utilisable par le composant de listing json du galaxia toolkit.
   * Soit une TestSuite filtrée à l'aide du composant SKF d'interprétation de l'ordre JSON issu de *Squash TM* exécutable par le moteur du framework.


2. En lot 2, ce composant effectuera également une analyse des dépendances python du projet sur une base à déterminer pour gérer le dépoiment
   des librairies python tierces.

3. En lot 3, ce composant acquiérera une logique d'extraction de métadonnées sur une base à déterminer en fonction d'une étude complémentaire sur les
   systèmes d'annotation praticables en python.

............
Plugin maven
............

1. En lot 1, ce module comprendra les fonctionnalités suivantes :

   * Mojo pour le goal ``list`` utilisé pour produire la liste des tests
     La prise en charge des métadonnées n'est pas prévue en lot 1

   * Mojo pour le goal ``run`` utilisé pour exécuter les tests
     Le code python des tests devra être autosuffisant (pas de librairies python tierces)

2. En lot 2, les améliorations suivantes seront faites :
   Le Mojo ``run`` deviendra capable de prendre en compte les dépendances python du projet (mécanisme à définir)

3. En lot 3, les améliorations suivantes seront faites :
   Le mojo ``list`` deviendra capable de présenter des métadonnées selon un mécanisme à définir lors d'une étude complémentaire.
   L'objectif sera si possible d'utiliser un mécanisme d'annotation conforme aux outils usuels en python.'

.. [#isCompound] Cette macro enchaîne l'appel des deux autres

.. [#cfgExecFW] La configuration est un option File (type file) avec les propriétés suivantes:

   * **testName**      : nom du test.
   * **suiteSource** [#added4Phase1Proto]_: chemin vers le fichier .robot de la suite qui contient le test.


.. [#added4Phase1Proto] Paramètre ajouté dans le cadre de la réalisation du prototype de phase 1
