<?xml version="1.0" encoding="UTF-8"?>
<!--

        This file is part of the Squashtest platform.
        Copyright (C) 2019 - 2020 Henix

        See the NOTICE file distributed with this work for additional
        information regarding copyright ownership.

        This is free software: you can redistribute it and/or modify
        it under the terms of the GNU Lesser General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        this software is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Lesser General Public License for more details.

        You should have received a copy of the GNU Lesser General Public License
        along with this software.  If not, see <http://www.gnu.org/licenses />.

-->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>org.squashtest.ta</groupId>
        <artifactId>squash-ta-parent</artifactId>
        <version>1.8.2020-RELEASE</version>
    </parent>
    <groupId>org.squashtest.ta.galaxia</groupId>
    <artifactId>squash-tf-robotframework-runner</artifactId>
    <version>1.0.1-SNAPSHOT</version>
    <packaging>pom</packaging>
    <inceptionYear>2019</inceptionYear>
    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
        <addon.toolkit.version>1.2.0-RELEASE</addon.toolkit.version>
        <skf.version>1.15.0.IT12</skf.version>
        <maven.mojo.api>3.6.0</maven.mojo.api>
    </properties>
    <scm>
        <!-- Release will only properly work if we have these two (bug workaround) -->
        <connection>scm:git:${project.basedir}</connection>
        <developerConnection>scm:git:${project.basedir}</developerConnection>
        <tag>HEAD</tag>
    </scm>
    <build>
        <testSourceDirectory>src/test/java</testSourceDirectory>
        <plugins>
            <plugin>
                <groupId>com.mycila</groupId>
                <artifactId>license-maven-plugin</artifactId>
                <configuration>
                    <excludes combine.children="append">
                        <!-- Those ones come from the engine project, they've had a long road already ! -->
                        <exclude>src/main/scripts/Add_NixGitHooks.groovy</exclude>
                        <exclude>src/main/scripts/AddWindowsGitHooks.groovy</exclude>
                        <exclude>src/main/scripts/precommit.sh</exclude>
                        <exclude>**/requirements.txt</exclude>
                     </excludes>
                </configuration>
                <inherited>false</inherited>
                <executions>
                    <execution>
                        <goals>
                            <goal>check</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>license-ception-in-scripts</id>
			<goals>
                            <goal>check</goal>
                        </goals>
                        <configuration combine.children="override">
                            <excludes combine.self="override" />
                            <includes>
                                <include>src/main/scripts/Add_NixGitHooks.groovy</include>
                                <include>src/main/scripts/AddWindowsGitHooks.groovy</include>
                                <include>src/main/scripts/precommit.sh</include>
                            </includes>
                            <properties>
                                <license.yearSpan>2011 - ${licence.current.year}</license.yearSpan>
                            </properties>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <profiles>
        <profile>
            <id>docbuild-linux</id>
            <activation>
                <os>
                    <family>linux</family>
                </os>
                <file>
                    <exists>/usr/bin/sphinx-build</exists>
                </file>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.codehaus.mojo</groupId>
                        <artifactId>exec-maven-plugin</artifactId>
                        <version>1.6.0</version>
                        <inherited>false</inherited>
                        <executions>
                            <execution>
                                <id>doc-clean</id>
                                <goals>
                                    <goal>exec</goal>
                                </goals>
                                <phase>clean</phase>
                                <configuration>
                                    <basedir>documentation/docs</basedir>
                                    <executable>make</executable>
                                    <arguments>
                                        <argument>clean</argument>
                                    </arguments>
                                </configuration>
                            </execution>
                            <execution>
                                <id>doc-build</id>
                                <goals>
                                    <goal>exec</goal>
                                </goals>
                                <phase>compile</phase>
                                <configuration>
                                    <basedir>documentation/docs</basedir>
                                    <executable>make</executable>
                                    <arguments>
                                        <argument>html</argument>
                                    </arguments>
                                </configuration>
                            </execution>
                            <execution>
                                <id>index-deploy</id>
                                <goals>
                                    <goal>exec</goal>
                                </goals>
                                <phase>package</phase>
                                <configuration>
                                    <basedir>.</basedir>
                                    <executable>cp</executable>
                                    <arguments>
                                        <argument>src/main/site/index.html</argument>
                                        <argument>documentation/docs/_build/index.html</argument>
                                    </arguments>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <groupId>com.mycila</groupId>
                        <artifactId>license-maven-plugin</artifactId>
                        <configuration>
                            <excludes combine.self="append">
		                <!-- Those ones come from the engine project, they've had a long road already ! -->
                                <exclude>src/main/scripts/Add_NixGitHooks.groovy</exclude>
                                <exclude>src/main/scripts/AddWindowsGitHooks.groovy</exclude>
                                <exclude>src/main/scripts/precommit.sh</exclude>
                            </excludes>
                        </configuration>
                        <executions>
                            <execution>
                                <id>license-ception-in-scripts</id>
                                <configuration combine.children="override">
                                    <excludes combine.self="override" />
                                    <includes>
                                        <include>src/main/scripts/Add_NixGitHooks.groovy</include>
                                        <include>src/main/scripts/AddWindowsGitHooks.groovy</include>
                                        <include>src/main/scripts/precommit.sh</include>
                                    </includes>
                                    <properties>
                                        <license.yearSpan>2011 - ${licence.current.year}</license.yearSpan>
                                    </properties>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
        <profile>
            <id>add-windows-hg-hooks</id>
            <activation>
                <os>
                    <family>windows</family>
                </os>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.codehaus.gmaven</groupId>
                        <artifactId>groovy-maven-plugin</artifactId>
                        <version>2.1.1</version>
                        <executions>
                            <execution>
                                <id>add-windows-hg-hooks</id>
                                <goals>
                                    <goal>execute</goal>
                                </goals>
                                <phase>process-resources</phase>
                                <inherited>false</inherited>
                                <configuration>
                                    <source>${project.basedir}/src/main/scripts/AddWindowsGitHooks.groovy</source>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
         <profile>
            <id>add-unix-like-hg-hooks</id>
            <activation>
                <os>
                    <family>unix</family>
                </os>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.codehaus.gmaven</groupId>
                        <artifactId>groovy-maven-plugin</artifactId>
                        <version>2.1.1</version>
                        <executions>
                            <execution>
                                <id>aadd-unix-like-hg-hooks</id>
                                <goals>
                                    <goal>execute</goal>
                                </goals>
                                <phase>process-resources</phase>
                                <inherited>false</inherited>
                                <configuration>
                                    <source>${project.basedir}/src/main/scripts/Add_NixGitHooks.groovy</source>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>
    <modules>
        <module>squash-tf-robotframework-runner-exploitation</module>
        <module>squash-tf-robotframework-runner-maven-plugin</module>
    </modules>
    <distributionManagement>
        <repository>
          <id>squash-release-deploy-repo</id>
          <name>Squash releases deployment repo</name>
          <url>${deploy-repo.release.url}</url>
        </repository>
        <snapshotRepository>
          <id>squash-snapshot-deploy-repo</id>
          <name>Squash snapshot deployment repo</name>
          <url>${deploy-repo.snapshot.url}</url>
        </snapshotRepository>
    </distributionManagement>    
</project>
